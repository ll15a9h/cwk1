// Class for COMP2931 Coursework 1

package comp2931.cwk1;
import java.util.Calendar;

/**
 * Simple representation of a date.
 */
public class Date  {
    
  private int year;
  private int month;
  private int day;
  private int[] monthsDays= {31,28,31,30,31,30,31,31,30,31,30,31};

    /**
     * <p>
     * Constructor for the Date class which uses calender library to get the current date
     * </p>
     */
    public Date(){
        Calendar cal = Calendar.getInstance();
        day = cal.get(Calendar.DAY_OF_MONTH);
        month = cal.get(Calendar.MONTH) + 1;
        year = cal.get(Calendar.YEAR);

    }

    /**
   * Creates a date using the given values for year, month and day.
   *
   * @param y Year
   * @param m Month
   * @param d Day
   */
    public Date(int y, int m, int d) {
    year = y;
    month = m;
    day = d;

        /**
         * check if dates are valid and within there limtitaons
         */
    if (y<1){

      throw new IllegalArgumentException("invalid year");
    }
      //Test leap year
      if (year%400 == 0 || (year% 4 == 0 && year%100 !=0)){
          monthsDays[1] = 29;
      }

    if (m < 1 || m > 12){

      throw new IllegalArgumentException("invalid Month");
    }
    if (d > monthsDays[m-1] || d < 1 ) {

      throw new IllegalArgumentException("invalid Days");

    }

  }

    /**
     *
     * @return The the value of the day in placement of the calender e.g January 31st returns 31
     */
    public int getDayOfYear() {

        int dayOfYear = 0;

        for (int i = 1; i < month; i++) {
            dayOfYear = dayOfYear + monthsDays[i - 1];
            // System.out.println(dayOfYear );
        }
        dayOfYear += day;
        //date = dayOfYear;
        return dayOfYear;
    }

    /**
     * Returns the year component of this date.
     *
     * @return Year
     */

    public int getYear() {
        return year;
    }

    /**
     * Returns the month component of this date.
     *
     * @return Month
     */
    public int getMonth() {
        return month;
    }

    /**
     * Returns the day component of this date.
     *
     * @return Day
     */
    public int getDay() {
        return day;
    }

    /**
     * Provides a string representation of this date.
     * <p>
     * ISO 8601 format is used (YYYY-MM-DD).
     *
     * @return Date as a string
     */

    @Override
    public String toString() {
        return String.format("%04d-%02d-%02d", year, month, day);
    }


    /**
     *
     * @param other checks if the value is the same as and if not return false
     * @return the day which consists of using the getters to gather the date information
     */
    @Override
    public boolean equals(Object other) {
        if (other == this) {
            // 'other' is same object as this one!
            return true;
        } else if (!(other instanceof Date)) {
            // 'other' is not a Time object
            return false;
        } else {
            // Compare fields
            Date otherDate = (Date) other;
            return getDay() == otherDate.getDay()
                    && getMonth() == otherDate.getMonth()
                    && getYear() == otherDate.getYear();
        }
    }
}


