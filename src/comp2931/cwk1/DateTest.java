package comp2931.cwk1;


import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;

/**
 * Created by hasnatabdul on 02/11/2017.
 */

public class DateTest {

    private Date dateTest;

    @Before
    public void setUp() {
        dateTest = new Date(2011, 6, 15);

    }
    @Test
    public void dateToString(){
        assertThat(dateTest.toString(), is ("2011-06-15"));
        assertThat(dateTest.toString(), is (not("2010-6-15")));
        assertThat(dateTest.toString(), is (not("2011-6-15")));
    }

    @Test(expected=IllegalArgumentException.class)
    public void dayTooHigh(){
        new Date(2011, 6, 32);
    }

    @Test(expected=IllegalArgumentException.class)
    public void dayTooLow(){
        new Date(2011, 3, -2);

    }

    @Test(expected=IllegalArgumentException.class)
    public void MonthTooHigh(){
        new Date(2010, 13, 15);
    }

    @Test(expected=IllegalArgumentException.class)
    public void MonthTooLow(){
        new Date(2012, -1, 15);
    }

    @Test(expected=IllegalArgumentException.class)
    public void yearToolow(){
        new Date(-1133, 6, 15);
    }

    @Test
    public void equality() {

        assertTrue(dateTest.equals(dateTest));
        assertTrue(dateTest.equals(new Date(2011, 6, 15)));     //Test existing date
        assertFalse(dateTest.equals(new Date(2011, 7, 15)));    //Test m+1
        assertFalse(dateTest.equals(new Date(2011,6 , 16)));    //test d+1
        assertFalse(dateTest.equals(new Date(2012, 7, 15)));    //test y+1
        assertFalse(dateTest.equals("2011-06-15"));                        //test string
    }

    @Test
    public void dateToNumber(){

        Date feb1 = (new Date(2013, 2, 1));
        Date dec31 = (new Date(2013, 12, 31));
        Date leap = (new Date(2004, 12, 31));

        assertThat(feb1.getDayOfYear(), is (32));
        assertThat(dec31.getDayOfYear(), is (365));
        assertThat(dec31.getDayOfYear(), is (not(364)));
        assertThat(leap.getDayOfYear(), is (366));

        assertThat(dateTest.getDayOfYear(), is (166));
    }

    @Test
    public void isLeapYear(){
        new Date(2004, 2, 29); //check date is valid
        new Date(2000, 2, 29);
    }

//    @Test
//    public void currentDate(){
//        Date today = new Date();
//        assertThat(today.toString(), is ("2017-11-08"));
//
//    }

}